/*Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.*/

import { writable, derived } from 'svelte/store';
import type { Channel } from './typings'

export const channels: Channel[] = [
	{ 
		name: "Lobby",
		prompts: [
			"What's your hobby?",
			"Do you play any sports? If so, what kind?",
			"What's the most exciting thing that's happened today?"
		]
	},
	{ 
		name: "Science",
		prompts: [
			"What is your current velocity?",
			"What is your favorite compound?"
		]
	},
	{
		name: "Math",
		prompts: []
	},
	{ 
		name: "History",
		prompts: []
	},
	{ 
		name: "English",
		prompts: [
			"What's your favorite book?",
			"What's your favorite quote?",
			"What are you currently reading or watching?"
		]
	},
	{ 
		name: "Elective",
		prompts: []
	},
	{ 
		name: "Language",
		prompts: [
			"What's your favorite language?",
			"What's your favorite word?"
		]
	}
]

export const channelNumber = writable((() => {

	const lessHashedHash = location.hash.substring(1, location.hash.length)

	if (!lessHashedHash) return 0

	if (!isNaN(lessHashedHash as unknown as number)) {
		return parseInt(lessHashedHash)
	} else {
		return 0
	}
})())

channelNumber.subscribe(number => {
	location.hash = number.toString()
})

export const channel = derived(
	channelNumber,
	$channelNumber => channels[$channelNumber]
);

export const userInput = writable("")
