/* Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.*/

const lorem = "Do you have the notes from last class? Science Physics Math English Spanish, Electives awesome Where is my phone? Class is starting very shortly. Does anyone have the answers to last year's math homework? Is anyone interested in playing some fighting games later on today? Is anyone participating in homecoming for 9th 10th 11th 12th grade? Does anyone have a spare chromebook laptop charger? It's very hot cold outside! I wish it would just stop raining. Did you hear?"
	.split(" ")
export const randomLorem = () => lorem.sort(() => .5 - Math.random())
	.slice(0, Math.floor(Math.random() * (20 - 1)) + 1)
	.join(" ")
