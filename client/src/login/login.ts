/* Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.*/

import { writable, Writable } from 'svelte/store';
import { client } from '../socket'

export const profile: Writable<string | null> = writable(
	localStorage.getItem("credential")
);

client.on("invalid", () => {
	profile.set(null);
	location.reload()
})

profile.subscribe((value: any) => {
	
	if (value === null) {
		localStorage.removeItem("credential")
		return
	}

	localStorage.setItem("credential", value)
	
	client.emit("profile", value);
});
