/*Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.*/

import { io, Socket } from "socket.io-client";
import type { DeleteMessageRequest, EditMessageRequest, Message as MessageType } from './messages/typings';
import { writable } from 'svelte/store'

interface ClientToServerEvents {
	channel: (channelNumber: number) => void;
	profile: (profile: any) => void;
	deleteMessage: (message_id: string) => void;
	reportMessage: (message_id: string) => void;
	reportUser: (user_id: string) => void;
	editMessage: (data: { message_id: string, message: string } ) => void;
	message: (message: string) => void;
	channelPromptChange: (prompt: string) => void;
}

interface ServerToClientEvents {
	username: (usernameResponse: string) => void;
	invalid: () => void;
	admin: () => void;
	message: (message: MessageType) => void;
	deleteMessage: (messageRequest: DeleteMessageRequest) => void;
	channelPromptChange: (prompt: string) => void;
	editMessage: (messageRequest: EditMessageRequest) => void;
}

export const client : Socket<ServerToClientEvents, ClientToServerEvents> = io()

export const admin = writable(false)

client.on("admin", () => admin.set(true))
