/* Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.*/

// TODO dear god (from old files, found a markdown parser + escaper)
// TODO im not sure if such a god exists if this was made in the first place
export default function(str: string): string {

	str = str
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")

	try {
		str = str.replace(/(\*\*\*)([^*]*)(\*\*\*)/g, (item) => `<b><i>${item.substring(3, item.length - 3)}</i></b>`)
			.replace(/(\*\*)([^*]*)(\*\*)/g, (item) => `<b>${item.substring(2, item.length - 2)}</b>`)
			.replace(/(\*)([^*]*)(\*)/g, (item) => `<i>${item.substring(1, item.length - 1)}</i>`)
			.replace(/(\~\~)([^*]*)(\~\~)/g, (item) => `<s>${item.substring(2, item.length - 2)}</s>`)
			.replace(/(https?:\/\/[^\s]+.\w+)/g, (url) => `<a href="${url}" target="_blank">${url}</a>`);
	} catch (e) {
		console.error(e);
	}
	return str;
}
