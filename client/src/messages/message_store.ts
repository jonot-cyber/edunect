/* Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.*/

import { writable, Writable } from 'svelte/store';
import { channel } from '../store'
import { client } from '../socket'
import type { Message } from './typings'

export const messages: Writable<Message[]> = writable([]) 
export const prompt: Writable<string | undefined> = writable(undefined)

client.on("channelPromptChange", (channelPrompt: string) => {
	prompt.set(channelPrompt);
})

channel.subscribe($channel => {
	if ($channel.prompts.length == 0) return undefined

	prompt.set($channel.prompts[Math.floor(Math.random()*$channel.prompts.length)])
})
