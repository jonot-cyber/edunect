"""
Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.
"""

from pymongo import MongoClient
import logging

logger = logging.getLogger('database')

class DB:

	client: MongoClient

	def __init__(self, url: str):
		if url == "mongodb://":
			self.client = None
			return
		try:
			self.client = MongoClient(url)
			self.client.server_info()
			# messagedb is the main message database
			self.db = self.client.messagedb
		except:
			logger.error("Can not connect to database.")

			self.client = None
	
	def savemsg(self, message, user, channel):

		if (self.client == None):
			return

		# Each collection name is the channel number
		msgcollection = self.db[channel]
		insert = msgcollection.insert_one({
			"author": user.username,
			"author_id": user.id,
			"message": message.message,
			"time": message.time,
			"message_id": message.message_id
		})

	def delmsg(self, message_id, channel):

		if (self.client == None):
			return

		msgcollection = self.db[channel]
		query = {"message_id": message_id}
		msgcollection.delete_one(query)

	#TODO: Implement fetching messages

