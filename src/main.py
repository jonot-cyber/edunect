"""
Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.
"""

from flask import Flask, send_from_directory, request
from flask_socketio import SocketIO, emit, join_room, leave_room
from database import DB
from user import User, users
from message import Message
from dotenv import load_dotenv
from google.oauth2 import id_token
from google.auth.transport import requests
from auth import is_user, is_admin

import time
import os

load_dotenv()

app = Flask(__name__)
app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY") or os.urandom(24)

#TODO: Implement proper database error handling
db = DB("mongodb://")

# TODO: don't allow every single origin (possible impersonation)
socketio = SocketIO(app,cors_allowed_origins="*")

# Route main page
@app.route("/")
def home_page():
	return send_from_directory("../client/public", "index.html")

# Route for static files
@app.route("/<path:path>")
def base(path):
	return send_from_directory("../client/public", path)

# SocketIO handlers
@socketio.on('profile')
def on_profile(profile):
	""" Runs when a client sends their profile to the server """

	try:
		idinfo = id_token.verify_oauth2_token(profile, requests.Request(), "126205909469-pimp11m3ukq2i7cmhv3ieracrmn8ovi2.apps.googleusercontent.com")
	except:
		emit("invalid")
		return
		
	if not is_user(idinfo["email"]):
		emit("invalid")
		return
	
	if is_admin(idinfo["email"]):
		emit("admin")

	users[request.sid] = User(request.sid, idinfo["name"].title(), idinfo["email"].lower(), idinfo["picture"])
	join_room(1)

@socketio.on('disconnect')
def on_disconnect():
	""" Runs when a client disconnects from the server """

	if request.sid not in users:
		return

	del users[request.sid]

@socketio.on("channel")
def on_channel(num):
	""" Runs when a client switches their channel """

	if request.sid not in users:
		return

	user = users[request.sid]
	
	leave_room(user.current_room + 1)
	join_room(num + 1)

	user.current_room = num

@socketio.on("deleteMessage")
def on_delete_message(message_id):
	""" Deletes the message associated with that id """

	if request.sid not in users:
		return

	user = users[request.sid]
	
	db.delmsg(message_id, str(user.current_room))

	

	emit(
		"deleteMessage",
		{"author": user.json, "message_id": message_id, "admin": is_admin(user.email) },
		to=user.current_room + 1
	)

@socketio.on("channelPromptChange")
def on_channel_prompt_change(channel_prompt):
	if request.sid not in users:
		return
	
	user = users[request.sid]

	if not is_admin(user.email):
		return

	emit("channelPromptChange", channel_prompt, to=user.current_room + 1)

@socketio.on("editMessage")
def on_edit_message(edit_message_content):
	""" Edits the message associated with a message ID """

	if request.sid not in users:
		return

	user = users[request.sid]

	emit(
		"editMessage",
		{ 
			"author": user.json, 
			"message_id": edit_message_content["message_id"],
			"new_message": edit_message_content["message"],
			"time": time.time() * 1000
		},
		to=user.current_room + 1
	)

@socketio.on("message")
def on_message(string):
	""" Runs when the client sends a message in the chat """
	if not string:
		raise Warning("Message does not exist")

	if len(string) > 2000:
		raise Warning("Message exceeds maximum length")

	if request.sid not in users:
		return

	user = users[request.sid]
	message = Message( 
			author=user,
			message=string,
			time=time.time() * 1000
	)

	db.savemsg(message, user, str(user.current_room))
	emit(
		"message",
		message.json,
		broadcast=True,
		to=user.current_room + 1
	)

if __name__ == "__main__":
	socketio.run(app,port=3000)
