"""
Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.
"""

from user import User
from dataclasses import dataclass, field
import string
import random
import jsons

def random_id():
	""" Create a random string with a length of 30 """
	return ''.join(random.choices(string.ascii_uppercase + string.digits, k=30))

@dataclass
class Message(jsons.JsonSerializable):
	""" Represents a user """
	author: User
	message: str
	time: float
	message_id: str = field(default_factory=random_id)
