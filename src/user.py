"""
Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.
"""

from dataclasses import dataclass, field
import jsons

@dataclass
class User(jsons.JsonSerializable):
	"""represents a user"""
	user_id: str
	username: str
	email: str = ""
	avatar_link: str = ""
	current_room: int = field(repr=False, default=0)

#Raised error "TypeError: 'type' object is not subscriptable"
users: 'dict[str, User]' = {}
