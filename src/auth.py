"""
Copyright 2021 @jonot-cyber

This file is part of Edunect.

Edunect is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Edunect is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Edunect.  If not, see <https://www.gnu.org/licenses/>.
"""

from typing import Iterable, List
import toml
import os
import re

CONFIGURATION_PATH = "./configuration.toml"

if not os.path.exists(CONFIGURATION_PATH):
    raise Exception("Configuration file not found")

configuration_file = open(CONFIGURATION_PATH, "r")
configuration = configuration_file.read()

configuration = toml.loads(configuration)
user_domains = configuration["authentication"]["allowed_email"]
admin_domains = configuration["authentication"]["admin_email"]

configuration_file.close() # always close your file buffers.

def get_email_domain(email: str) -> str:
    """ get the domain of an email """
    if not re.match(r"^[^@]+@[^@]+$", email): # thats one fun regex ^_^
        # is not a valid email address
        return ""
    parts = email.split("@")

    return parts[1].lower() # email addresses are case sensitive

def lowercase(strings: List[str]) -> Iterable:
    """ function for internal use only """
    return (x.lower() for x in strings)

def is_user(address: str) -> bool:
    """ checks if email domain is allowed for users """
    domain = get_email_domain(address)

    allowed_domains = user_domains + admin_domains

    if len(allowed_domains) == 0:
        return True
    
    # email addresses are case sensitive
    allowed_domains = lowercase(allowed_domains)

    return domain in allowed_domains

def is_admin(address: str) -> bool:
    """ checks if email domain is allowed for admins """
    domain = get_email_domain(address)

    allowed_domains = admin_domains

    if len(allowed_domains) == 0:
        return True

    # email addresses are case sensitive
    allowed_domains = lowercase(allowed_domains)

    return domain in allowed_domains
