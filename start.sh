#!/bin/bash

# a simple bash script to build svelte files and start the server (start for glitch)
(cd client; npm run dev) &

# python 3.6-3.8 is still supported and works fine with all of our code. there is no reason to force python 3.9
python3 src/main.py

# force kill all node processes after interrupt
killall -9 node
fuser -k 3000/tcp
