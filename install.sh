#!/bin/bash

# a simple bash script to install pip/node files
(cd client; npm install)
# python 3.6-3.8 is still supported and works fine with all of our code. there is no reason to force python 3.9
python3 -m pip install -r requirements.txt
